<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package stroso
 */

get_header();
?>

	<section id="error-container" class="full-width clearfix" data-bg="navy-blue">
		<div class="container" data-padding="10015">
			<div class="row">
				<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 man_intro_cont">
					<div class="man_404">404</div>
					<h1>SOMETHING'S GONE WRONG</h1>
					<p>We are very sorry but the page you are looking for cannot be found.</p> 
					<a href="javascript:history.back()" class="btn base-btn"><i class="ti ti-arrow-left"></i> Back</a>
				</div>
				<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
					<img src="http://stroso.local/wp-content/uploads/2018/11/404-error.png" class="img-responsive pull-right" />
				</div>
			</div>
		</div>
	</section>

<?php
get_footer();
