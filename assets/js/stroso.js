jQuery(function($) {
  var $body = jQuery("body");
  var $window = jQuery(window);

  jQuery(document).ready(function() {
    jQuery(this).scrollTop(0);
  });
  var shrinkHeader = 250;

  jQuery(window).scroll(function() {
    var scroll = getCurrentScroll();
    if (scroll >= shrinkHeader) {
      jQuery("#main-header-section").addClass("shrink");
    } else {
      jQuery("#main-header-section").removeClass("shrink");
    }
  });
  function getCurrentScroll() {
    return window.pageYOffset || document.documentElement.scrollTop;
  }

  jQuery("#menu-ham").click(function() {
    jQuery(this).toggleClass("is-active");

    jQuery("#mobile-nav").toggleClass("is-active");
  });

  jQuery("#mobile-nav li.menu-item-has-children a").click(function() {
    jQuery("#mobile-nav .sub-menu").hide();
    jQuery(this)
      .parent("li")
      .children(".sub-menu")
      .toggle();
  });
});
