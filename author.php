<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package stroso
 */

get_header();
?>

<?php if ( have_posts() ) : ?>

<?php
	// Set the Current Author Variable $curauth
	$curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));
?>

	<section id="posts-author-details-container" class="full-width clearfix" data-bg="black">
		<div class="container no-padding-bottom" data-padding="5015">
			<div class="row">
				<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 author-profile">
					<div class="author-photo">
						<?php echo get_avatar( $curauth->user_email , '150'); ?>
					</div>
				</div>
				<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 author-details">
					<div class="author-details-cont">
						<h3><?php echo $curauth->nickname; ?></h3>
						<p><?php echo $curauth->user_description; ?></p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="posts-archive-container" class="full-width clearfix" data-bg="light-gray">

		<div class="container" data-padding="5015">
			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 main posts-list">
					<div class="row">
						<?php while ( have_posts() ) : the_post(); ?>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 post-item">
								<?php if(has_post_thumbnail()): ?>
									<div class="post-thumbnail-block">
										<a href="<?php the_permalink(); ?>">
											<?php the_post_thumbnail('full', array('class' => 'img-responsive')); ?>
										</a>
									</div>
								<?php endif; ?>
								<div class="post-info">
									<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
									<div class="blog-meta">
										<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><i class="fa fa-user"></i>by <?php the_author(); ?> </a>
										<a><i class="fa fa-calendar"></i><?php echo get_the_date('M Y'); ?></a>
										<?php if(wp_count_comments($post->ID)->total_comments <= 1) { $comments_count = "No Comments"; } else{ $comments_count = wp_count_comments($post->ID)->total_comments; } ?>
										<a><i class="fa fa-comment"></i><?php echo $comments_count; ?> </a>
									</div>
									<p><?php echo excerpt(65); ?></p>
									<a href="<?php the_permalink(); ?>" class="btn btn-blog">read more
										<i class="fa fa-long-arrow-right"></i>
									</a>
								</div>
							</div>
						<?php endwhile; ?>
					</div>
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 archive-navigation">
							<?php cs_numeric_posts_nav(); ?>
						</div>
					</div>

				</div>
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 sidebar">
					<?php get_sidebar(); ?>
				</div>
			</div>
		</div>
	</section>

<?php endif; ?>

<?php
get_footer();
