<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package stroso
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="comments-area">

	<?php
	// You can start editing here -- including this comment!
	if ( have_comments() ) :
		?>
		<h2 class="comments-title">
			<?php
			$scrawk_comment_count = get_comments_number();

			if($scrawk_comment_count < 1):
				echo "<span class='comment-count'>$scrawk_comment_count comments</span>";
			elseif($scrawk_comment_count < 2):
				echo "<span class='comment-count'>$scrawk_comment_count comment</span>";
			else:
				echo "<span class='comment-count'>$scrawk_comment_count comments</span>";
			endif;
			
			?>
		</h2><!-- .comments-title -->

		<?php the_comments_navigation(); ?>

		<ol class="comment-list">
			<?php
			wp_list_comments( array(
				'style'      => 'ol',
				'short_ping' => true,
			) );
			?>
		</ol><!-- .comment-list -->

		<?php
		the_comments_navigation();

		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() ) :
			?>
			<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'scrawk' ); ?></p>
			<?php
		endif;

	endif; // Check for have_comments().

	
$args = array(
    'fields' => apply_filters(
        'comment_form_default_fields', array(
            'author' =>'<p class="comment-form-author">' . '<input id="author" placeholder="Your Name (No Keywords)" name="author" type="text" value="' .
                esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' />'.
                '</p>'
                ,
            'email'  => '<p class="comment-form-email">' . '<input id="email" placeholder="your-real-email@example.com" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .
                '" size="30"' . $aria_req . ' />'  .
                '</p>',
            'url'    => '<p class="comment-form-url">' .
             '<input id="url" name="url" placeholder="http://your-site-name.com" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" /> ' .
               '</p>'
        )
    ),
    'comment_field' => '<p class="comment-form-comment">' .
        '<label for="comment">' . __( 'Let us know what you have to say:' ) . '</label>' .
        '<textarea id="comment" name="comment" placeholder="Express your thoughts, idea or write a feedback by clicking here & start an awesome comment" cols="45" rows="8" aria-required="true"></textarea>' .
        '</p>',
    'comment_notes_after' => '',
    'title_reply' => '<div class="crunchify-text"> <h5>Please Post Your Comments & Reviews</h5></div>'
);

		comment_form( $args, $post_id, array('title_reply'=>'Got Something To Say:') );
	?>

</div><!-- #comments -->
