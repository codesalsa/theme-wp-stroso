<?php
/**
 * stroso functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package stroso
 */

if ( ! function_exists( 'stroso_setup' ) ) :

	function stroso_setup() {

		load_theme_textdomain( 'stroso', get_template_directory() . '/languages' );

		add_theme_support( 'automatic-feed-links' );

		add_theme_support( 'title-tag' );

		add_theme_support( 'post-thumbnails' );

		register_nav_menus( array(
			'primary' => esc_html__( 'Primary', 'stroso' ),
			'servicemenu' => esc_html__( 'Service Menu', 'stroso' ),
			'mobilemenu' => esc_html__( 'Mobile Menu', 'stroso' ),
			'footer_links' => esc_html__( 'Footer Links', 'stroso' ),
		) );

		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		add_theme_support( 'custom-background', apply_filters( 'stroso_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		add_theme_support( 'customize-selective-refresh-widgets' );

	}
endif;
add_action( 'after_setup_theme', 'stroso_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function stroso_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'stroso_content_width', 640 );
}
add_action( 'after_setup_theme', 'stroso_content_width', 0 );


function stroso_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'stroso' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'stroso' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'stroso_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function stroso_scripts() {
	wp_enqueue_style('stroso-bootstrap-style', get_stylesheet_directory_uri().'/vendors/bootstrap/css/bootstrap.min.css');
	wp_enqueue_style('stroso-bootstrap-theme-style', get_stylesheet_directory_uri().'/vendors/bootstrap/css/bootstrap-theme.min.css');
	wp_enqueue_style('stroso-fa-style', get_stylesheet_directory_uri().'/vendors/fonts/font-awesome/css/font-awesome.min.css');
	wp_enqueue_style('stroso-flaticon-style', get_stylesheet_directory_uri().'/vendors/fonts/flat-icon/flaticon.css');
	wp_enqueue_style('stroso-ham-style', get_stylesheet_directory_uri().'/vendors/hamburgers/hamburgers.css');
	wp_enqueue_style('stroso-animations-style', get_stylesheet_directory_uri().'/vendors/animations/animate.css');
	wp_enqueue_style('stroso-custom-style', get_stylesheet_directory_uri().'/assets/css/stroso.css');

	wp_enqueue_script( 'stroso-bootstrap-script', get_stylesheet_directory_uri() . '/vendors/bootstrap/js/bootstrap.min.js', array( 'jquery' ) );
	wp_enqueue_script( 'stroso-custom-script', get_stylesheet_directory_uri() . '/assets/js/stroso.js', array( 'jquery' ) );
}
add_action( 'wp_enqueue_scripts', 'stroso_scripts' );


/**
 * Load Admin styles
*/

function stroso_load_admin_style() {
    wp_enqueue_style( 'stroso_admin_css', get_stylesheet_directory_uri() . '/assets/css/stroso-admin.css');
    wp_enqueue_style( 'stroso_admin_fa_css', get_stylesheet_directory_uri() . '/vendors/fonts/font-awesome/css/font-awesome.min.css');
}
add_action( 'admin_enqueue_scripts', 'stroso_load_admin_style' );

/**
 * stroso Custom header hook
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * stroso Template tags hook
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * stroso Template functions hook.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * stroso Template Custom Post types.
 */
require get_template_directory() . '/inc/template-cpt.php';

/**
 * stroso Plugin Activation Class
 */
require_once locate_template('/lib/stroso-plugin-activation/class-stroso-plugin-activation.php');
require get_template_directory() . '/lib/stroso-plugin-activation/template-required-plugin.php';

/**
 * stroso Template Options hook
 */
require get_template_directory() . '/lib/stroso-options-framework/template-options.php';

/**
 * stroso Shortcode hook
 */
require get_template_directory() . '/inc/template-shortcodes.php';


