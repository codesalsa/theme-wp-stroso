<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package stroso
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>
		<?php 
			if ( of_get_option( 'site_title' ) ) { 
				echo of_get_option( 'site_title' );
			} else{
				bloginfo("name"); ?> | <?php bloginfo("description"); 
			}
		?>
	</title>
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php if ( of_get_option( 'site_description' ) ) { ?>
		<meta name="description" content="<?php echo of_get_option( 'site_description' ); ?>">
	<?php } ?>
		
	<?php if ( of_get_option( 'site_keywords' ) ) { ?>
		<meta name="keywords" content="<?php echo of_get_option( 'site_keywords' ); ?>">
	<?php } ?>

	<?php if ( of_get_option( 'favicon_uploader' ) ) { ?>
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo of_get_option( 'favicon_uploader' ); ?>">
	<?php } ?>

	<?php wp_head(); ?>
</head>

<body <?php body_class('yellow-basecolor'); ?>>

	<section id="top-header-section" class="full-width clearfix hidden-sm hidden-xs" data-bg="black">
		<div class="container" data-padding="1515">
			<div class="row">
				<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 topbar-left">
					<ul>
						<li><a href="#"><i class="fa fa-envelope"></i> info@stroso.com</a></li>
						<li><a href="#"><i class="fa fa-phone"></i> 123-4567-890</a></li>
						<li><a href="#"><i class="fa fa-map-marker"></i> House-5,1/3 avenew,Newyork</a></li>
					</ul>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 topbar-right socials ">
					<ul class="header-social-icons">
						<?php if ( of_get_option('social_fb') ) { ?>
							<li class="facebook">
								<a target="_blank" href="<?php echo of_get_option('social_fb'); ?>"><i class="fa fa-facebook"></i></a>
							</li>
						<?php } ?>
						<?php if ( of_get_option('social_twitter') ) { ?>
							<li class="twitter">
								<a target="_blank" href="<?php echo of_get_option('social_twitter'); ?>"><i class="fa fa-twitter"></i></a>
							</li>
						<?php } ?>
						<?php if ( of_get_option('social_linkedin') ) { ?>
							<li class="linkedin">
								<a target="_blank" href="<?php echo of_get_option('social_linkedin'); ?>"><i class="fa fa-linkedin"></i></a>
							</li>
						<?php } ?>
						<?php if ( of_get_option('social_gplus') ) { ?>
							<li class="gplus">
								<a target="_blank" href="<?php echo of_get_option('social_gplus'); ?>"><i class="fa fa-google-plus"></i></a>
							</li>
						<?php } ?>
						<?php if ( of_get_option('social_youtube') ) { ?>
							<li class="youtube">
								<a target="_blank" href="<?php echo of_get_option('social_youtube'); ?>"><i class="fa fa-youtube"></i></a>
							</li>
						<?php } ?>
						<?php if ( of_get_option('social_pintrest') ) { ?>
							<li class="pinterest">
								<a target="_blank" href="<?php echo of_get_option('social_pintrest'); ?>"><i class="fa fa-pinterest"></i></a>
							</li>
						<?php } ?>
						<?php if ( of_get_option('social_instagram') ) { ?>
							<li class="instagram">
								<a target="_blank" href="<?php echo of_get_option('social_instagram'); ?>"><i class="fa fa-instagram"></i></a>
							</li>
						<?php } ?>
					</ul>
				</div>
			</div>
		</div>
	</section>

	<header id="main-header-section" class="full-width clearfix" data-bg="navy-blue">
		<div class="container">
			<div class="row">
				<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 header-branding" data-padding="1515">
					<a class="branding" href="<?php echo esc_url( home_url( '/' ) ); ?>">
						<?php if ( of_get_option('logo_uploader') ) { ?> 

							<img src="<?php echo of_get_option('logo_uploader'); ?>" class="img-responsive" />

						<?php } ?>
					</a>
				</div>
				<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 header-right">
					<nav class="primary-nav" data-position="right">
						<?php wp_nav_menu( array( 'items_wrap' => '<ul class="hidden-xs hidden-sm">%3$s</ul>', 'theme_location' => 'primary') ); ?>
					</nav>
				</div>
			</div>
		</div>
	</header>

	<section class="mobile-menu-cont hidden-lg hidden-md">
		<div id="menu-ham" class="hamburger hamburger--elastic hidden-lg hidden-md">
			<span class="hamburger-box">
				<span class="hamburger-inner"></span>
			</span>
		</div>
		<div id="mobile-nav" class="hidden-lg hidden-md">
			<?php wp_nav_menu( array( 'items_wrap' => '<ul>%3$s</ul>', 'theme_location' => 'mobilemenu') ); ?>
		</div>
	</section>
