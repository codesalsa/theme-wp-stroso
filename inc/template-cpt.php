<?php
/*========================================*/
/* Create a Servicescpt post type */
/*========================================*/
add_action( 'init', 'create_services_post_type' );
 
function create_services_post_type() {
    $args = array(
                  'description' => 'Services',
                  'show_ui' => true,
                  'menu_position' => 93,
				  'menu_icon' => 'dashicons-awards',
                  'exclude_from_search' => true,
                  'labels' => array(
						'name'=> 'Services',
						'singular_name' => 'Service',
						'add_new' => 'Add New Service',
						'add_new_item' => 'Add New Service',
						'edit' => 'Edit Service',
						'edit_item' => 'Edit Service',
						'new-item' => 'New Service',
						'view' => 'View Service',
						'view_item' => 'View Service',
						'search_items' => 'Search Service',
						'not_found' => 'No Service Found',
						'not_found_in_trash' => 'No Service Found in Trash',
						'parent' => 'Parent Service'
					   ),
	 'public' => true,
	 'capability_type' => 'post',
	 'hierarchical' => false,
	 'rewrite' => array( 'slug' => 'serviceposts', 'with_front' => false ),  
	 'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'comments'),
	 'has_archive' => true,
	 );

	register_post_type( 'servicescpt' , $args );
	
}

/*========================================*/
/* Create a Testimonialscpt post type */
/*========================================*/
add_action( 'init', 'create_testimonials_post_type' );
 
function create_testimonials_post_type() {
    $args = array(
                  'description' => 'Testimonials',
                  'show_ui' => true,
                  'menu_position' => 94,
				  'menu_icon' => 'dashicons-id-alt',
                  'exclude_from_search' => true,
                  'labels' => array(
						'name'=> 'Testimonials',
						'singular_name' => 'Testimonial',
						'add_new' => 'Add New Testimonial',
						'add_new_item' => 'Add New Testimonial',
						'edit' => 'Edit Testimonial',
						'edit_item' => 'Edit Testimonial',
						'new-item' => 'New Testimonial',
						'view' => 'View Testimonial',
						'view_item' => 'View Testimonial',
						'search_items' => 'Search Testimonial',
						'not_found' => 'No Testimonial Found',
						'not_found_in_trash' => 'No Testimonial Found in Trash',
						'parent' => 'Parent Testimonial'
					   ),
	 'public' => true,
	 'capability_type' => 'post',
	 'hierarchical' => false,
	 'rewrite' => true,
	 'supports' => array('title', 'editor', 'thumbnail'),
	 'has_archive' => true,
	 );

	register_post_type( 'testimonialscpt' , $args );
	
}

/*========================================*/
/* Create a Teamcpt post type */
/*========================================*/
add_action( 'init', 'create_teams_post_type' );
 
function create_teams_post_type() {
    $args = array(
                  'description' => 'Teams',
                  'show_ui' => true,
                  'menu_position' => 95,
				  'menu_icon' => 'dashicons-groups',
                  'exclude_from_search' => true,
                  'labels' => array(
						'name'=> 'Teams',
						'singular_name' => 'Team',
						'add_new' => 'Add New Team Member',
						'add_new_item' => 'Add New Team Member',
						'edit' => 'Edit Team Member',
						'edit_item' => 'Edit Team Member',
						'new-item' => 'New Team Member',
						'view' => 'View Team Member',
						'view_item' => 'View Team Member',
						'search_items' => 'Search Team Member',
						'not_found' => 'No Team Member Found',
						'not_found_in_trash' => 'No Team Member Found in Trash',
						'parent' => 'Parent Team Member'
					   ),
	 'public' => true,
	 'capability_type' => 'post',
	 'hierarchical' => false,
	 'rewrite' => true,
	 'supports' => array('title', 'editor', 'thumbnail'),
	 'has_archive' => true,
	 );

	register_post_type( 'teamscpt' , $args );
	
}


/*========================================*/
/* Create a Partnerscpt post type */
/*========================================*/
add_action( 'init', 'create_partners_post_type' );
 
function create_partners_post_type() {
    $args = array(
                  'description' => 'Partners',
                  'show_ui' => true,
                  'menu_position' => 95,
				  'menu_icon' => 'dashicons-universal-access',
                  'exclude_from_search' => true,
                  'labels' => array(
						'name'=> 'Partners',
						'singular_name' => 'Partner',
						'add_new' => 'Add New Partner',
						'add_new_item' => 'Add New Partner',
						'edit' => 'Edit Partner',
						'edit_item' => 'Edit Partner',
						'new-item' => 'New Partner',
						'view' => 'View Partner',
						'view_item' => 'View Partner',
						'search_items' => 'Search Partner',
						'not_found' => 'No Partner Found',
						'not_found_in_trash' => 'No Partner Found in Trash',
						'parent' => 'Parent Partner'
					   ),
	 'public' => true,
	 'capability_type' => 'post',
	 'hierarchical' => false,
	 'rewrite' => true,
	 'supports' => array('title', 'editor', 'thumbnail'),
	 'has_archive' => true,
	 );

	register_post_type( 'partnerscpt' , $args );
	
}

/*========================================*/
/* Create a pricing post type */
/*========================================*/
add_action( 'init', 'create_pricing_post_type' );
 
function create_pricing_post_type() {
    $args = array(
                  'description' => 'Pricing Plans',
                  'show_ui' => true,
                  'menu_position' => 95,
				  'menu_icon' => 'dashicons-chart-bar',
                  'exclude_from_search' => true,
                  'labels' => array(
						'name'=> 'Pricing Plans',
						'singular_name' => 'Plan Item',
						'add_new' => 'Add New Plan Item',
						'add_new_item' => 'Add New  Plan Item',
						'edit' => 'Edit  Plan Item',
						'edit_item' => 'Edit  Plan Item',
						'new-item' => 'New  Plan Item',
						'view' => 'View  Plan Item',
						'view_item' => 'View  Plan Item',
						'search_items' => 'Search  Plan Item',
						'not_found' => 'No  Plan Item Found',
						'not_found_in_trash' => 'No  Plan Item Found in Trash',
						'parent' => 'Parent  Plan Item'
					   ),
	 'public' => true,
	 'capability_type' => 'post',
	 'hierarchical' => false,
	 'rewrite' => true,
	 'supports' => array('title', 'editor', 'thumbnail'),
	 'has_archive' => true,
	 );

	register_post_type( 'pricingcpt' , $args );
	
}