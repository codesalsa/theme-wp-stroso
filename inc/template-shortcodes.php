<?php
/* ==================== */
/* Stroso Shortcodes */
/* ==================== */

/* Services CPT shortcode */
add_shortcode( 'services-cpt','stroso_services_cpt_list' );
function stroso_services_cpt_list ( $atts ) {
  $atts = shortcode_atts( array(
    'class' => ''
  ), $atts );
    //$terms = get_terms('product_category');
    wp_reset_query();
    $args = array(
        'post_type' => 'servicescpt',
        'order'     => 'DESC'
    );
     $loop = new WP_Query($args);
     if($loop->have_posts()) { 
        echo "<div class='row service-items'>";
        while($loop->have_posts()) : $loop->the_post(); ?>

            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 service-item">
                <div class="service-item-cont">
                    <div class="col-sm-3">
                        <?php
                            $iconType = get_field('service_icon_type');
                            $icon = get_field('service_icon');

                            if($iconType == 'FontAwesome Icon'): ?>
                                <div class="icon-cont">
                                    <i class="fa <?php echo $icon; ?>"></i>
                                </div>
                            <?php elseif($iconType == 'Icon Image'): ?>
                                <div class="icon-cont">
                                    <img src="<?php echo $icon; ?>" class="img-responsive" />
                                </div>
                        <?php endif; ?>
                    </div>
                    <div class="col-sm-9">
                        <div class="txt-cont">
                            <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                            <p><?php echo excerpt(10); ?></p>
                        </div>
                    </div>
                </div>
            </div>
    <?php
        endwhile;
        echo "</div>";
     }

     else {
            echo  'Sorry, no posts were found';
          }
}