<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package stroso
 */

get_header();
?>

<section id="hero-section" class="full-width clearfix bg-img-section"
<?php if ( of_get_option('homepage_banner_overlay') ): ?>data-overlaycolor="<?php echo of_get_option('homepage_banner_overlay'); ?>"<?php endif; ?>
<?php if ( of_get_option('homepage_banner_overlay_opacity') ): ?>data-overlayopacity="<?php echo of_get_option('homepage_banner_overlay_opacity'); ?>"<?php endif; ?>
	<?php if ( of_get_option('homepage_banner_img') ): ?> style="background: url(<?php echo of_get_option('homepage_banner_img'); ?>) no-repeat center center;background-size:cover;" <?php endif; ?>>
	<div class="container" data-padding="10015">
		<div class="row">
			<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 text-left animated fadeInUp">
				<?php if ( of_get_option('homepage_banner_txt') ): ?>
					<?php echo of_get_option('homepage_banner_txt'); ?>
				<?php endif; ?>
			</div>
		</div>
	</div>

</section>
<?php if ( of_get_option( 'service_module_enable' ) ) : ?>
	<section id="services-section" class="full-width clearfix" data-bg="light-blue">
		<div class="container" data-padding="10015">
			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 service-title animated fadeInLeft">
				<?php if ( of_get_option( 'service_module_title' ) ) : ?>	
					<h2 class="section-title">
						<?php echo of_get_option( 'service_module_title' ); ?>
					</h2>
				<?php endif; ?>
				<?php if ( of_get_option( 'service_module_desc' ) ) : ?>
					<p><?php echo of_get_option( 'service_module_desc' ); ?></p>
				<?php endif; ?>
				<?php if ( of_get_option( 'service_module_cta_txt' ) ) : ?>
					<a href="#" class="btn btn-large"><?php echo of_get_option( 'service_module_cta_txt' ); ?></a>
				<?php endif; ?>
				</div>
				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
					<div class="row service-items">
					<?php 
						query_posts(array( 
							'order' => 'DESC',
							'post_type' => 'servicescpt',
							'showposts' => 4,
						) );  
					?>
					<?php while ( have_posts() ) : the_post(); ?>	
					<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 service-item animated fadeInRight">
							<div class="service-item-cont">
								<div class="col-sm-3">
									<?php
										$iconType = get_field('service_icon_type');
										$icon = get_field('service_icon');

										if($iconType == 'FontAwesome Icon'): ?>
											<div class="icon-cont">
												<i class="fa <?php echo $icon; ?>"></i>
											</div>
										<?php elseif($iconType == 'Icon Image'): ?>
											<div class="icon-cont">
												<img src="<?php echo $icon; ?>" class="img-responsive" />
											</div>
									<?php endif; ?>
								</div>
								<div class="col-sm-9">
									<div class="txt-cont">
										<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
										<p><?php echo excerpt(10); ?></p>
									</div>
								</div>
							</div>
						</div>
						<?php endwhile; ?>
						<?php wp_reset_query(); ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>
<?php if ( of_get_option( 'partners_module_enable' ) ) : ?>
<section id="partners-container" class="full-width clearfix" data-bg = "white">
	<div class="container" data-padding="10015">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">
				<?php if ( of_get_option( 'partners_module_title' ) ) : ?>	
					<h2 class="section-title">
						<?php echo of_get_option( 'partners_module_title' ); ?>
					</h2>
				<?php endif; ?>
				<?php if ( of_get_option( 'partners_module_desc' ) ) : ?>
					<p class="sub-txt"><?php echo of_get_option( 'partners_module_desc' ); ?></p>
				<?php endif; ?>
				<?php if ( of_get_option( 'partners_module_cta_txt' ) ) : ?>
					<a href="#" class="btn btn-large"><?php echo of_get_option( 'partners_module_cta_txt' ); ?></a>
				<?php endif; ?>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<div class="row partners">
					<?php 
						query_posts(array( 
							'order' => 'DESC',
							'showposts' => 6,
							'post_type' => 'partnerscpt',
						) );  
					?>
					<?php while ( have_posts() ) : the_post(); ?>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 partner-item">
						<?php
							$image = get_field('partner_logo');

							if( !empty($image) ): ?>
							<a href="<?php echo get_field('partner_url'); ?>" class="partner-logo">
								<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							</a>
						<?php endif; ?>
					</div>
					<?php endwhile; ?>
					<?php wp_reset_query(); ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>
<section id="quote-container" class="full-width clearfix" data-bg = "white">
	<div class="container-fluid" data-padding="0">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 free-consult-container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
						<h2 class="section-title">Free Consultation!</h2>
						<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 contact-form">
						<?php echo do_shortcode("[contact-form-7 id='96' title='Request a Quote']"); ?>
					</div>
				</div>
			</div>

			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 why-people-container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
						<h2 class="section-title">Why People Choose Us</h2>
						<p>Lorem ipsum dolor sit amet, magna phasellus augue, blandit metus cus,enas eltum. Non neque, a eu mauris arcu volutpat.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
						<ul>
							<li>
								<div class="icon"><i class="flaticon-idea"></i></div>
								<div class="cont">
									<h4>Free Business Advice</h4>
									<p>Aermentum sed vestibulum dui commodo nunc lorem nunc sapien. Ut elit sed felis</p>
								</div>
							</li>
							<li>
								<div class="icon"><i class="flaticon-money"></i></div>
								<div class="cont">
									<h4>Subject Matter Experts</h4>
									<p>Aermentum sed vestibulum dui commodo nunc lorem nunc sapien. Ut elit sed felis</p>
								</div>
							</li>
							<li>
								<div class="icon"><i class="flaticon-support"></i></div>
								<div class="cont">
									<h4>Support 24/7</h4>
									<p>Aermentum sed vestibulum dui commodo nunc lorem nunc sapien. Ut elit sed felis</p>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="pricing-container" class="full-width clearfix" data-bg = "dark-blue">
	<div class="container" data-padding="10015">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">

				<h2 class="section-title">Affordable Price Plan</h2>
				<p class="sub-txt">Demonstr averunt lectores legere me lius quod qua pro legunt saepius Claritas est etiam pro cessus dynamicus qui sequitur.</p>

			</div>
		</div>
		<div class="row">
			<?php 
				query_posts(array( 
					'order' => 'DESC',
					'post_type' => 'pricingcpt',
					'showposts' => 3,
				) );  
			?>
			<?php while ( have_posts() ) : the_post(); ?>
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 padding-0">
				<div class="pricing-box <?php if(get_field('is_premium_plan')): ?>premium-plan<?php endif; ?>">
					<h3><?php the_title(); ?></h3>
					<div class="price-block">
						<h2>
						<?php if(get_field('plan_currency')): ?>
							<small><?php echo get_field('plan_currency'); ?></small>
						<?php endif; ?>
						<?php if(get_field('plan_price')): ?>
							<?php echo get_field('plan_price'); ?>
						<?php endif; ?>
						<?php if(get_field('plan_type')): ?>
							<span><?php echo get_field('plan_type'); ?></span>
						<?php endif; ?>
						</h2>
					</div>
					<?php the_content(); ?>
					<?php if(get_field('plan_cta')): ?>
						<a href="<?php echo get_field('plan_cta_url'); ?>" class="btn btn-outline"><?php echo get_field('plan_cta'); ?></a>
					<?php endif; ?>
				</div>
			</div>
			<?php endwhile; ?>
			<?php wp_reset_query(); ?>
		</div>
	</div>
</section>

<section id="blogs-container" class="full-width clearfix" data-bg = "light-gray">
	<div class="container" data-padding="10015">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
				<h2 class="section-title">Recent Blog</h2>
				<p class="sub-txt">Demonstr averunt lectores legere me lius quod qua pro legunt saepius Claritas est etiam pro cessus dynamicus qui sequitur mutationem consuetudium lec torum. Mirum notare quam littera gothica, quam the nunc.</p>
			</div>
		</div>
		<div class="row blog-posts">
			<?php 
				query_posts(array( 
					'order' => 'DESC',
					'showposts' => 3,
					'category_name' => 'blog'
				) );  
			?>
			<?php while ( have_posts() ) : the_post(); ?>
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
				<article class="blog-items">
					<?php if(has_post_thumbnail()): ?>	
						<div class="blog-img">
							<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('medium', array('class' => 'img-responsive')); ?></a>
						</div>
					<?php endif; ?>
					<div class="blog-content">
						<h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
						<div class="blog-meta">
							<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><i class="fa fa-user"></i>by <?php the_author(); ?> </a>
							<a><i class="fa fa-calendar"></i><?php echo get_the_date('M Y'); ?></a>
							<?php if(wp_count_comments($post->ID)->total_comments <= 1) { $comments_count = "No Comments"; } else{ $comments_count = wp_count_comments($post->ID)->total_comments; } ?>
							<a><i class="fa fa-comment"></i><?php echo $comments_count; ?> </a>
						</div>
						<p><?php echo excerpt(25); ?></p>
						<a href="<?php the_permalink(); ?>" class="btn btn-blog">read more
							<i class="fa fa-long-arrow-right"></i>
						</a>
					</div>
				</article>
			</div>
			<?php endwhile; ?>
			<?php wp_reset_query(); ?>
		</div>
	</div>
</section>

<section id="testimonials-container" class="full-width clearfix" data-bg="white">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
				<h2 class="section-title">What our customers say</h2>
				<p class="sub-txt"> Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Mauris elementum mauris vitae tortor. Sed vel lectus. Donec odio tempus molestie, porttitor ut, iaculis quis, sem. Fusce dui leo, imperdiet in, aliquam sit amet, feugiat eu, orci.</p>
			</div>
		</div>
		<div class="row testimonials">
			<?php 
				query_posts(array( 
					'order' => 'DESC',
					'post_type' => 'testimonialscpt',
					'showposts' => 3,
				) );  
			?>
			<?php while ( have_posts() ) : the_post(); ?>
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 testi-items">
					<?php if(has_post_thumbnail()): ?>	
						<div class="testi-img">
							<?php the_post_thumbnail(); ?>
						</div>
					<?php endif; ?>
					<div class="blockquote-wrap text-center">
						<blockquote class="quotes">
							<p><?php echo excerpt(15); ?></p>
							<div class="block-quote-meta">
								<span class="client-name"><?php the_title(); ?></span>
								<small class="designation">
									<?php echo get_field('client_designation'); ?>
								</small>
								<span class="company">
									<?php echo get_field('client_company'); ?>
								</span>
								<span class="rating">
									<?php for($x = 1; $x <= get_field('client_rating'); $x++){
										echo "<i class='fa fa-star'></i>";
									} ?>
								</span>
							</div>
							
						</blockquote>
					</div>
				</div>
			<?php endwhile; ?>
			<?php wp_reset_query(); ?>
		</div>
	</div>
</section>

<section id="teams-container" class="full-width clearfix" data-bg = "light-gray">
	<div class="container" data-padding="5015">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
				<h2 class="section-title">Meet our Team</h2>
				<p class="sub-txt"> Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Mauris elementum mauris vitae tortor. Sed vel lectus. Donec odio tempus molestie, porttitor ut, iaculis quis, sem. Fusce dui leo, imperdiet in, aliquam sit amet, feugiat eu, orci.</p>
			</div>
		</div>
		<div class="row teams">
			<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 teams-list">
				<div class="row">
					<?php 
						query_posts(array( 
							'order' => 'DESC',
							'post_type' => 'teamscpt',
							'showposts' => 4,
						) );  
					?>
					<?php while ( have_posts() ) : the_post(); ?>
					<?php 
						$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' ); 
						$member_socials = get_field('member_social_profile'); 
					?>
					<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 team-item">
						<div class="team-item-cont">
							<?php if(has_post_thumbnail()): ?>	
								<div class="team-item-img">
									<?php the_post_thumbnail('full', array('class' => 'img-responsive')); ?>
								</div>
							<?php endif; ?>
							<span class="team-item-title"><?php the_title(); ?></span>
							<div class="show-team-details" style="">
								<div class="team-item-details-cont">
									<span class="team-item-title"><a href="#"><?php the_title(); ?></a></span>
									<span class="team-item-desig">
										<?php echo get_field('member_designation'); ?>
									</span>
									<div class="team-item-detail-txt">
										<?php echo excerpt(15); ?>
									</div>
									<?php 
										$member_socials = get_field('member_social_profile'); 
										if( $member_socials ):
									?>
									<div class="team-item-socials">
										<?php if($member_socials['facebook_url']): ?>
											<a href="<?php echo $member_socials['facebook_url']; ?>" class="fb-profile"><i class="fa fa-facebook"></i></a>
										<?php endif; ?>
										<?php if($member_socials['twitter_url']): ?>
											<a href="<?php echo $member_socials['twitter_url']; ?>" class="twitter-profile"><i class="fa fa-twitter"></i></a>
										<?php endif; ?>
										<?php if($member_socials['linkedin_url']): ?>
											<a href="<?php echo $member_socials['linkedin_url']; ?> class="linkedin-profile""><i class="fa fa-linkedin"></i></a>
										<?php endif; ?>
										<?php if($member_socials['google_plus_url']): ?>
											<a href="<?php echo $member_socials['google_plus_url']; ?>" class="gplus-profile"><i class="fa fa-google-plus"></i></a>
										<?php endif; ?>

									</div>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
					<?php endwhile; ?>
					<?php wp_reset_query(); ?>
				</div>
			</div>

		</div>
	</div>
</section>

<section class="promo-container full-width clearfix" data-bg = "black">
	<div class="container" data-padding="5015">
		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
				<h3>Last go with us for your feature business</h3>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
				<a href="#" class="btn base-btn">Purchase</a>
			</div>
		</div>
	</div>
</section>

<section id="about-container" class="full-width clearfix" data-bg="white">
	<div class="container-fluid">
		<div class="row">
			<?php
			    $query = new WP_Query( 'pagename=about-us' );
				while ( $query->have_posts() ) : $query->the_post();
				if ( has_post_thumbnail() ):
					$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
				endif;
			?>
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left info-cont">
				<h2 class="section-title"><?php the_title(); ?></h2>
				<p class="sub-txt"><?php echo excerpt(65); ?></p>
				<a href="<?php the_permalink(); ?>" class="btn base-btn">Learn More</a>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" <?php if ( has_post_thumbnail() ): ?> style="background: url('<?php echo $thumb['0']; ?>') no-repeat center center;background-size:cover;" <?php endif; ?>>

			</div>
			<?php endwhile; ?>
			<?php wp_reset_query(); ?>
		</div>
	</div>
</section>

	
<?php
get_footer();
