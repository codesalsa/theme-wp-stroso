<?php
/*
 * Loads the Options Panel
 *
 * If you're loading from a child theme use stylesheet_directory
 * instead of template_directory
 */

define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_stylesheet_directory_uri() . '/lib/stroso-options-framework/inc/' );
require_once dirname( __FILE__ ) . '/inc/options-framework.php';

/*
 * This is an example of how to add custom scripts to the options panel.
 * This one shows/hides the an option when a checkbox is clicked.
 *
 * You can delete it if you not using that option
 */
add_action( 'optionsframework_custom_scripts', 'optionsframework_custom_scripts' );

function optionsframework_custom_scripts() { ?>

<script type="text/javascript">
jQuery(document).ready(function() {

	jQuery('#example_showhidden').click(function() {
  		jQuery('#section-example_text_hidden').fadeToggle(400);
	});

	if (jQuery('#example_showhidden:checked').val() !== undefined) {
		jQuery('#section-example_text_hidden').show();
	}

});
</script>

<?php
}

/**
 * function stroso_change_theme_options_menu_name()
 * changes the name of the menu item of theme options framework
 */
function stroso_change_theme_options_menu_name( $menu ) {
	
	/* alter the options menu paramters */
	$menu[ 'menu_title' ] = 'Stroso Options'; // set the menu title
	$menu[ 'page_title' ] = 'Stroso Options'; // set the menus page title
	$menu[ 'mode' ] = 'menu'; // make the menu a top level menu item
	$menu[ 'position' ] = '81'; // make the menu item appear after settings
	//$menu[ 'icon_url' ] = 'dashicons-admin-settings'; // Custom Menu icon image
	$menu['menu_slug'] = 'stroso-options';
	
	/* return our modified menu */
	return $menu;
	
}
add_filter( 'optionsframework_menu', 'stroso_change_theme_options_menu_name' );