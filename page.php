<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package stroso
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

	<section id="page-container" class="full-width clearfix">
		<div class="container" data-padding="5015">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 main">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</section>

<?php endwhile; ?>

<?php
get_footer();
