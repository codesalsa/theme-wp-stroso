<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package stroso
 */

get_header();
?>

<?php if ( have_posts() ) : ?>

<section id="posts-archive-container" class="full-width clearfix" data-bg="light-gray">

	<div class="container" data-padding="5015">
		
		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 main posts-list">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<header class="page-header">
							<h1 class="page-title">
								<?php
								/* translators: %s: search query. */
								printf( esc_html__( 'Search Results for: %s', 'scrawk' ), '<span>' . get_search_query() . '</span>' );
								?>
							</h1>
						</header><!-- .page-header -->
					</div>
				</div>
				<div class="row">
					<?php while ( have_posts() ) : the_post(); ?>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 post-item">
							<?php if(has_post_thumbnail()): ?>
								<div class="post-thumbnail-block">
									<a href="<?php the_permalink(); ?>">
										<?php the_post_thumbnail('full', array('class' => 'img-responsive')); ?>
									</a>
								</div>
							<?php endif; ?>
							<div class="post-info">
								<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
								<div class="blog-meta">
									<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><i class="fa fa-user"></i>by <?php the_author(); ?> </a>
									<a><i class="fa fa-calendar"></i><?php echo get_the_date('M Y'); ?></a>
									<?php if(wp_count_comments($post->ID)->total_comments <= 1) { $comments_count = "No Comments"; } else{ $comments_count = wp_count_comments($post->ID)->total_comments; } ?>
									<a><i class="fa fa-comment"></i><?php echo $comments_count; ?> </a>
								</div>
								<p><?php echo excerpt(65); ?></p>
								<a href="<?php the_permalink(); ?>" class="btn btn-blog">read more
									<i class="fa fa-long-arrow-right"></i>
								</a>
							</div>
						</div>
					<?php endwhile; ?>
				</div>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 archive-navigation">
						<?php cs_numeric_posts_nav(); ?>
					</div>
				</div>

			</div>
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 sidebar">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
</section>

<?php endif; ?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php if ( have_posts() ) : ?>

			

			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'template-parts/content', 'search' );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_sidebar();
get_footer();
