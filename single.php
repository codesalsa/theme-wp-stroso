<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package stroso
 */

get_header();
?>
<?php while ( have_posts() ) : the_post(); ?>

<section id="page-container" class="page-single full-width clearfix">
	<div class="container" data-padding="5015">
		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 breadcrumb">
				<?php the_breadcrumb(); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 main">
				<h1 class="single-title"><?php the_title(); ?></h1>
				
				<?php if(has_post_thumbnail()): ?>
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 post-thumbnail-block">
							<?php the_post_thumbnail('full', array('class' => 'img-responsive')); ?>			
						</div>
					</div>
				<?php endif; ?>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 post-content">
						<?php the_content(); ?>
					</div>
				</div>
				<div class="row comments-section">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<?php 
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;
						?>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 sidebar">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
</section>

<?php endwhile; ?>

<?php
get_footer();
